package com.stailgames.main;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.stailgames.manager.Assets;
import com.stailgames.manager.R;
import com.stailgames.screen.GameScreen;

public class Main extends Game {

	public static String TAG = Main.class.getName();

	public static String NAME = "Flying Cockroach";
	public static int VERSION = 1;

	private Assets assets;

	@Override
	public void create() {
		assets = new Assets();
		// loading assets
		assets.load();
		while (assets.loading()) {
			Gdx.app.log(TAG, "Loading : " + assets.progress());
		}
		Gdx.app.log(TAG, "Done");
		// located the assets
		R.provide(assets);
		setScreen(new GameScreen());
	}

}
