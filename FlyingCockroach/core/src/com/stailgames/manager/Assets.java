package com.stailgames.manager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Disposable;

public class Assets implements Disposable, AssetErrorListener {

	private static final String TAG = Assets.class.getName();

	private AssetManager manager;

	public Assets() {
		manager = new AssetManager();
		manager.setErrorListener(this);
	}

	public void load() {
		manager.load("flyingcockroach.atlas", TextureAtlas.class);
	}

	@Override
	public void error(@SuppressWarnings("rawtypes") AssetDescriptor asset, Throwable throwable) {
		Gdx.app.error(TAG, "Couldn't load asset " + asset.fileName + " : " + (Exception) throwable);
	}

	@Override
	public void dispose() {
		manager.dispose();
	}

	public boolean loading() {
		return !manager.update();
	}

	public float progress() {
		return manager.getProgress();
	}

	public Texture logo() {
		Texture logo = manager.get("logo.png", Texture.class);
		logo.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		return logo;
	}

	public AtlasRegion image(String filename) {
		return manager.get("flyingcockroach.atlas", TextureAtlas.class).findRegion(filename);
	}

	public Sound sound(String filename) {
		return null;
	}
	
	public Music music(String filename){
		return null;
	}

	public TextureAtlas fullimage() {
		return manager.get("flyingcockroach.atlas", TextureAtlas.class);
	}

}
