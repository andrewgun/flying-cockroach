package com.stailgames.components;

import static com.stailgames.utils.Converter.pixelToMeter;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.stailgames.components.entities.Movement;
import com.stailgames.manager.R;

public class Blade extends Component implements Poolable {
		
	private final static float WIDTH = pixelToMeter(40);
	private final static float HEIGHT = pixelToMeter(30);
	public final static float BASE_HEIGHT = pixelToMeter(30);
	
	public float x, y;
	public int unit;
	public boolean flip;
	
	public Blade() {
		super("Blade");
		x = 0.0f;
		y = 0.0f;
		flip = false;
	}

	public void create(float x, float y, int unit){
		create(x, y, unit, false);
	}
	
	public void create(float x, float y, int unit, boolean flip) {
		this.x = x;
		this.y = y;
		this.unit = unit;
		this.flip = flip;
		
		setBounding(new float[] { 
					x, y,
					x, y + (flip ? -BASE_HEIGHT : BASE_HEIGHT),
					x + WIDTH/2, y + (flip ? -BASE_HEIGHT : BASE_HEIGHT) + (flip ? -HEIGHT : HEIGHT),
					(x + (WIDTH * unit)) - WIDTH/2, y + (flip ? -BASE_HEIGHT : BASE_HEIGHT) + (flip ? -HEIGHT : HEIGHT),
					(x + (WIDTH * unit)), y + (flip ? -BASE_HEIGHT : BASE_HEIGHT),
					(x + (WIDTH * unit)), y
				 }
		);
	}

	@Override
	public void update(float delta) {
		x += Movement.platform;
		
		setBounding(new float[] { 
				x, y,
				x, y + (flip ? -BASE_HEIGHT : BASE_HEIGHT),
				x + WIDTH/2, y + (flip ? -BASE_HEIGHT : BASE_HEIGHT) + (flip ? -HEIGHT : HEIGHT),
				(x + (WIDTH * unit)) - WIDTH/2, y + (flip ? -BASE_HEIGHT : BASE_HEIGHT) + (flip ? -HEIGHT : HEIGHT),
				(x + (WIDTH * unit)), y + (flip ? -BASE_HEIGHT : BASE_HEIGHT),
				(x + (WIDTH * unit)), y
			 }
		);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		batch.draw(R.assets.image("block"), x, y, unit * WIDTH, flip ? -BASE_HEIGHT : BASE_HEIGHT);
		for (int i = 0; i < unit; i++)
			batch.draw(
					R.assets.image("triangle"), 
					x + (WIDTH * i), y + (flip ? -BASE_HEIGHT : BASE_HEIGHT), 
					WIDTH, flip ? -HEIGHT : HEIGHT);
	}


	@Override
	public boolean equals(Object blade) {
		return this.x == ((Blade) blade).x && this.y == ((Blade) blade).y;
	}	

	@Override
	public void reset() {
		x = 0.0f;
		y = 0.0f;
	}

}
