package com.stailgames.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class Score {

	public static final int MAX_SCORE = 99;

	public float x, y;

	public BitmapFont font;
	public int counter;

	public Score(float x, float y) {
		this.x = x;
		this.y = y;

		font = new BitmapFont(Gdx.files.internal("font-twcent-64.fnt"));
		counter = 0;
	}

	public void increase() {
		counter++;
		counter = counter < MAX_SCORE ? counter : MAX_SCORE;
	}

	public void draw(Batch batch) {
		String text = counter + "";
		font.draw(batch, text, x, y);
	}

}
