package com.stailgames.components.entities;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.stailgames.components.Live;
import com.stailgames.components.Score;
import com.stailgames.main.World;

public class HUD {

	private OrthographicCamera camera;
	private FillViewport viewport;
	private SpriteBatch batch;

	public Live liveBar;
	public Score scoreNumber;

	public HUD() {
		// SETTINGS
		camera = new OrthographicCamera();
		viewport = new FillViewport(World.WIDTH * World.PIXEL_PER_METER, World.HEIGHT * World.PIXEL_PER_METER, camera);
		camera.position.set(viewport.getWorldWidth() / 2, viewport.getWorldHeight() / 2, 0);
		camera.setToOrtho(false);
		camera.update();
		// CREATE NEW GL BATCH
		batch = new SpriteBatch();

		// COMPONENT
		liveBar = new Live(viewport.getWorldWidth() / 2 - 41.5f, 350f, 5);
		scoreNumber = new Score(385f, 320f);
	}

	public void render() {
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		liveBar.draw(batch);
		scoreNumber.draw(batch);
		batch.end();
	}

	public void resize(int width, int height) {
		viewport.update(width, height);
	}

}
