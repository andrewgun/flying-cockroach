package com.stailgames.components.entities;

import static com.badlogic.gdx.math.Intersector.intersectSegmentPolygon;
import static com.badlogic.gdx.math.Intersector.overlapConvexPolygons;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.stailgames.components.Blade;
import com.stailgames.components.Coins.Coin;
import com.stailgames.components.Fuel;
import com.stailgames.components.Heart;
import com.stailgames.components.Block;
import com.stailgames.components.Slipper;
import com.stailgames.components.Walls.Wall;
import com.stailgames.components.Windmill;

public class Detector extends Actor {

	// PLAYER
	private Cockroach cockroach;
	// GAME OBJECT
	private Environment environment;
	private Platform platform;
	private ThrowingObject object;

	// UI
	private HUD HUD;

	public Detector(Cockroach cockroach, Environment environment, Platform platform, HUD HUD, ThrowingObject object) {
		this.cockroach = cockroach;
		this.environment = environment;
		this.platform = platform;
		this.object = object;
		this.HUD = HUD;
	}

	public void response(float delta) {

		// OBSTALCE
		responseWall(delta, 0.1f);
		responseBlade(delta, 0.1f);
		responseWindmill(delta, 0.1f);
		responseSlipper(delta, 0.1f);
		// ITEM
		responseHeart();
		responseFuel();
		responseCoin();

		for (Block block : environment.getBlocks()) {
			Vector2 p1 = new Vector2(block.getX(), block.getY() + block.getHeight() + 0.3f);
			Vector2 p2 = new Vector2(block.getX() + block.getWidth(), block.getY() + block.getHeight() + 0.3f);
			if (intersectSegmentPolygon(p1, p2, cockroach.getBound())) {
				cockroach.setLimit(block.getY() + block.getHeight() + cockroach.getHeight() / 2,
						platform.getUpperLimit() - cockroach.getHeight() / 2);
				return;
			}
		}
		cockroach.setLimit(platform.getLowerLimit() + cockroach.getHeight() / 2,
				platform.getUpperLimit() - cockroach.getHeight() / 2);

		if (HUD.liveBar.getLive() == 0.0f) {
			Movement.stopAll();
		}

		responseAnimation();

	}

	private void responseAnimation() {
		if (isOverlapWall() || isOverlapBlade() || isOverlapWindmill()) {
			cockroach.setAnimation(true, 0.5f);
			return;
		}
		cockroach.setAnimation(false, 0.5f);
	}

	private float tickWall = 0.0f;

	private void responseWall(float delta, float delay) {
		if (isOverlapWall()) {
			if ((tickWall += delta) > delay) {
				HUD.liveBar.decrease(0.5f);
				tickWall = 0.0f;
			}
		}
	}

	private float tickWindmill = 0.0f;

	private void responseWindmill(float delta, float delay) {
		if (isOverlapWindmill()) {
			if ((tickWindmill += delta) > delay) {
				HUD.liveBar.decrease(0.5f);
				tickWindmill = 0.0f;
			}
		}
	}

	private float tickBlade = 0.0f;

	private void responseBlade(float delta, float delay) {
		if (isOverlapBlade()) {
			if ((tickBlade += delta) > delay) {
				HUD.liveBar.decrease(1.0f);
				tickBlade = 0.0f;
			}
		}
	}

	private float tickSlipper = 0.0f;

	private void responseSlipper(float delta, float delay) {
		if (isOverlapSlipper()) {
			if ((tickSlipper += delta) > delay) {
				HUD.liveBar.decrease(1.0f);
				tickSlipper = 0.0f;
			}
		}
	}

	private void responseHeart() {
		if (isOverlapHeart()) {
			HUD.liveBar.increase(1.0f);
		}
	}

	private void responseFuel() {
		if (isOverlapFuel()) {
			cockroach.getEnergy().setMAX();
		}
	}

	private void responseCoin() {
		if (isOverlapCoin()) {
			HUD.scoreNumber.increase();
		}
	}

	private boolean isOverlapWindmill() {
		for (Windmill windmill : environment.getWindmills()) {
			if (overlapConvexPolygons(cockroach.getBound(), windmill.getBound())) {
				return true;
			}
		}
		return false;
	}

	private boolean isOverlapCoin() {
		for (Coin coin : environment.getCoins()) {
			if (overlapConvexPolygons(cockroach.getBound(), coin.getBound())) {
				environment.removeCoin(coin);
				return true;
			}
		}
		return false;
	}

	private boolean isOverlapFuel() {
		for (Fuel fuel : environment.getFuels()) {
			if (overlapConvexPolygons(cockroach.getBound(), fuel.getBound())) {
				environment.removeFuel(fuel);
				return true;
			}
		}
		return false;
	}

	private boolean isOverlapHeart() {
		for (Heart heart : environment.getHearts()) {
			if (overlapConvexPolygons(cockroach.getBound(), heart.getBound())) {
				environment.removeHeart(heart);
				return true;
			}
		}
		return false;
	}

	private boolean isOverlapBlade() {
		for (Blade blade : platform.getBlades()) {
			if (overlapConvexPolygons(cockroach.getBound(), blade.getBound()))
				return true;
		}
		return false;
	}

	private boolean isOverlapWall() {
		for (Wall wall : environment.getWalls()) {
			if (overlapConvexPolygons(cockroach.getBound(), wall.getBound()))
				return true;
		}
		return false;
	}

	private boolean isOverlapSlipper() {
		for (Slipper slipper : object.getSlippers()) {
			if (overlapConvexPolygons(cockroach.getBound(), slipper.getBound())) {
				return true;
			}
		}
		return false;
	}

}
