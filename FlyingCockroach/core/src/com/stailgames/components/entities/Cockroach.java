package com.stailgames.components.entities;

import static com.stailgames.utils.Converter.colorByteToFloat;
import static com.stailgames.utils.Converter.pixelToMeter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.stailgames.components.Component;
import com.stailgames.main.World;
import com.stailgames.manager.R;
import com.stailgames.utils.Trail;
import com.stailgames.utils.Trail.Config;

public class Cockroach extends Component {

	// UTILITIES
	private static void drawCenter(Batch batch, TextureRegion reg, float x, float y, float width, float height,
			float rotation) {
		batch.draw(reg, x - width / 2, y - height / 2, width / 2, height / 2, width, height, 1f, 1f, rotation);
	}

	public static final TextureRegion[] IMAGES = R.assets.image("cockroach").split(50, 50)[0];
	public static final float X_START = 1.0f;
	public static final float Y_START = World.HEIGHT / 2;
	public static final float WIDTH = pixelToMeter(50);
	public static final float HEIGHT = pixelToMeter(50);

	public float x, y;

	// PHYSICS PARAMETER
	public float acceleration, velocity;

	// ROTATION PARAMETER
	private int rotation;

	// LIMIT PARAMETER
	private float minY;
	private float maxY;

	private Trail trail;
	private Energy energy;

	// ANIMATION PARAMETER
	public boolean animation;
	public float tick, duration;
	public TextureRegion image;

	// IMAGE PARAMETER
	public int frame;

	public boolean controller;

	public Cockroach() {
		super("Cockroach");
		set();
		// FEATURE
		Config config = new Config();
		config.color = new Color(colorByteToFloat(26), colorByteToFloat(26), colorByteToFloat(26), 1.0f);
		config.width = pixelToMeter(40);
		config.height = pixelToMeter(40);
		config.respawn = 0.1f;
		config.alphadecs = 0.2f;
		config.scaledecs = 0.2f;
		trail = new Trail(config);
		energy = new Energy();

		setBoundingRectangle(x - WIDTH / 2, y - HEIGHT / 2, WIDTH, HEIGHT);
	}

	private void set() {
		// POSITION
		x = X_START;
		y = Y_START;
		// PHYSICS
		velocity = 0.0f;
		acceleration = 0.0f;
		rotation = 0;
		// LIMIT
		minY = 0;
		maxY = World.HEIGHT;
		// ANIMATION
		animation = false;
		tick = 0.0f;
		duration = 0.1f;
		image = null;
		// IMAGE
		frame = 0;
		// CONTROLLER
		controller = true;
	}

	private boolean touch = false;

	private void controller() {
		Gdx.input.setInputProcessor(new InputAdapter() {

			@Override
			public boolean touchDown(int screenX, int screenY, int pointer, int button) {
				touch = true;
				return true;
			}

			@Override
			public boolean touchUp(int screenX, int screenY, int pointer, int button) {
				touch = false;
				return true;
			}

		});
	}

	@Override
	public void update(float delta) {
		controller();

		// PHYSICS UPDATE
		// acceleration by gravity
		acceleration = y == minY ? 0.0f : World.getGravity();
		velocity = MathUtils.clamp(velocity + (acceleration * delta), -0.1f, 0.1f);

		// POSITION UPDATE
		if (!isRotation())
			y += velocity;
		// limitation
		y = MathUtils.clamp(y, minY, maxY);

		// MECHANICS UPDATE
		if (controller) {
			if (touch) {
				if (energy.isEmpty()) {
					trail.stop();
				} else {
					// action give -> momentum : p = m.v (m = 1)
					velocity = 0.02f;
					energy.decrease(5f);
					trail.play();
				}
			} else
				trail.stop();
		} else
			trail.stop();

		if (y == minY && !isRotation())
			energy.increase(Movement.charging);

		// EFFECT UPDATE
		trail.create(x - WIDTH / 2, y - HEIGHT / 2, new Vector2(-0.02f, -0.01f));
		trail.update(delta);
		// ROTATION UPDATE
		angle = angle > -90 && angle < 90 ? angle + rotation : (rotation = 0);

		// IMAGE UPDATE
		image = animation ? IMAGES[(int) (tick += duration) % IMAGES.length] : IMAGES[frame];
		// UPDATE BOUND
		setBoundingRectangle(x - WIDTH / 2, y - HEIGHT / 2, WIDTH, HEIGHT);

	}

	public void setLimit(float minY, float maxY) {
		this.minY = minY;
		this.maxY = maxY;
	}

	public void setController(boolean state) {
		controller = state;
	}

	public void setFrame(int frame) {
		this.frame = frame;
	}

	public void setAnimation(boolean state, float duration) {
		animation = state;
		this.duration = duration;
	}

	public void turn90(int amount) {
		rotation = amount;
	}

	public void reset() {
		set();
		energy = new Energy();
	}

	public Energy getEnergy() {
		return energy;
	}

	public boolean isRotation() {
		return angle != 0;
	}

	public boolean isAnimated() {
		return animation;
	}

	public boolean isPOSatMinY() {
		return y == minY;
	}

	private int angle = 0;

	@Override
	public void draw(Batch batch, float parentAlpha) {
		drawCenter(batch, image, x, y, WIDTH, HEIGHT, angle);
		trail.draw(batch);
		energy.draw(batch, x - WIDTH / 2, y + 0.2f);
	}

	public class Energy {

		private static final float MAX_ENERGY = 1000;

		private float energy;

		public Energy() {
			setMAX();
		}

		public void increase(float amount) {
			energy = energy < MAX_ENERGY ? energy + amount : MAX_ENERGY;
		}

		public void setMAX() {
			energy = MAX_ENERGY;
		}

		public void decrease(float amount) {
			energy = energy > 0 ? energy - amount : 0f;
		}

		public boolean isEmpty() {
			return energy == 0;
		}

		public void draw(Batch batch, float x, float y) {
			batch.draw(R.assets.image("energy"), x, y, pixelToMeter(49) * (energy / 1000f), pixelToMeter(9));
			batch.draw(R.assets.image("energy_bar"), x, y, pixelToMeter(51), pixelToMeter(11));
		}

	}

}
