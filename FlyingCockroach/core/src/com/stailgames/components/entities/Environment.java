package com.stailgames.components.entities;

import static com.stailgames.utils.Converter.pixelToMeter;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.stailgames.components.Block;
import com.stailgames.components.Block.TYPE;
import com.stailgames.components.Coins;
import com.stailgames.components.Coins.Coin;
import com.stailgames.components.Coins.Pattern;
import com.stailgames.components.Component;
import com.stailgames.components.Fuel;
import com.stailgames.components.Heart;
import com.stailgames.components.Walls;
import com.stailgames.components.Walls.Wall;
import com.stailgames.components.Windmill;
import com.stailgames.main.World;

public class Environment extends Component {

	private static class CHANCE {
		
		public static float percent(float amount) {
			return amount / 100;
		}
		
		public static float WALL_FOR_MOTION = percent(30);
		public static float POWER_UP_TO_APPEAR = percent(70);
		private static float FUEL_TO_APPEAR = percent(70);
		
		public static float OBSTALCE_2ND_TO_APPEAR = percent(80);
		public static float BLOCK_CENTER_TO_APPEAR = percent(80);
		public static float COIN_BLOCK_TO_APPEAR = percent(80);
		
		public static float COIN_UPPER_OR_LOWER_TO_APPEAR = percent(50);
	}
	

	public static final float CLEARANCE = pixelToMeter(600);

	private Array<Component> obstacles = new Array<Component>();
	private Array<Component> items = new Array<Component>();

	private boolean active;
	private float pointer;

	public Environment() {
		super("Obstacle");
		active = true;
		pointer = World.WIDTH + 0.2f;
	}

	public void activated(boolean state) {
		active = state;
	}

	@Override
	public void update(float delta) {
		create();
		// UPDATE OBSTACLE
		for (Component obstacle : obstacles) {
			obstacle.update(delta);
		}
		// UPDATE ITEM
		for (Component item : items) {
			item.update(delta);
		}
		destroy();
	}

	private void create() {

		if (!active)
			return;
		if (obstacles.size == 0) {
			createWall(World.WIDTH + 0.2f, false);
		} else {
			if (pointer < World.WIDTH - CLEARANCE) {
				// 1st OBSTACLE

				if (MathUtils.randomBoolean(CHANCE.WALL_FOR_MOTION)) {
					createWall(World.WIDTH + 0.2f, true);
				} else {
					float position = createWall(World.WIDTH + 0.2f, false);

					// CHANCE POWER-UP TO APPEAR
					if (MathUtils.randomBoolean(CHANCE.POWER_UP_TO_APPEAR)) {
						// CHANCE FUEL TO APPEAR
						if (MathUtils.randomBoolean(CHANCE.FUEL_TO_APPEAR))
							createFuel(World.WIDTH + 0.2f, position + 0.02f);
						else
							createHeart(World.WIDTH + 0.2f, position + 0.02f);
					}
				}

				// 2nd OBSTACLE
				float blockPosition = 0.0f;

				// CHANCE 2ND OBSTACLE TO APPEAR
				if (MathUtils.randomBoolean(CHANCE.OBSTALCE_2ND_TO_APPEAR)) {

					// CHANCE BLOCK TO APPEAR
					if (MathUtils.randomBoolean(CHANCE.BLOCK_CENTER_TO_APPEAR)) {

						// RANDOM BLOCK POSITION
						blockPosition = MathUtils.randomBoolean() ? (World.HEIGHT / 2)
									  : MathUtils.randomBoolean() ? (World.HEIGHT / 3) : (World.HEIGHT * 2) / 3;

						createBlock(World.WIDTH + 0.2f + CLEARANCE / 2, blockPosition);

						if (MathUtils.randomBoolean(CHANCE.COIN_BLOCK_TO_APPEAR)) {
							// CHANCE COIN UNIT WILL BE APPEAR
							int unit = MathUtils.randomBoolean(CHANCE.percent(50)) ? 1
									 : MathUtils.randomBoolean(CHANCE.percent(30)) ? 0 : 3;

							createCoin(World.WIDTH + 0.2f + CLEARANCE / 2, blockPosition + 0.4f, unit, Pattern.CURVE);
						} else {
							createHeart(World.WIDTH + 0.2f + CLEARANCE / 2, blockPosition + 0.4f);
						}

					} else
						createWindmill(World.WIDTH + 0.2f + CLEARANCE / 2, World.HEIGHT / 2);
				}

				// CHANCE COIN TO APPEAR
				if (MathUtils.randomBoolean(CHANCE.COIN_UPPER_OR_LOWER_TO_APPEAR)) {
					// CHANCE COIN UNIT WILL BE APPEAR
					int unit = blockPosition == (World.HEIGHT * 2) / 3 ? 0 : MathUtils.randomBoolean(CHANCE.percent(50)) ? 1 : 0;

					createCoin(World.WIDTH + 0.2f + CLEARANCE / 2, 2.5f, unit, Pattern.LINE);
				} else {
					// CHANCE COIN UNIT WILL BE APPEAR
					int unit = MathUtils.randomBoolean(CHANCE.percent(50)) ? 1 : 0;

					createCoin(World.WIDTH + 0.2f + CLEARANCE / 2, 0.5f, unit, Pattern.LINE);
				}

				pointer = World.WIDTH + 0.2f;
			}
			pointer += Movement.environment;
		}
	}

	/**
	 * OBSTACLE
	 */

	private float createWall(float x, boolean motion) {
		Walls wall = poolWall.obtain();
		float y = Walls.POSITION.random().getValue();
		wall.create(x, y);
		if (motion)
			wall.setMotion(Walls.POSITION.BOTTOM.getValue(), Walls.POSITION.UPPER.getValue(), Movement.wall_motion);
		obstacles.add(wall);
		return y;
	}

	private void createBlock(float x, float y) {
		Block block = poolBlock.obtain();
		block.create(x, y, TYPE.ENVIRONMENT);
		block.setScale(0.5f, 1.0f);
		obstacles.add(block);
	}

	private void createWindmill(float x, float y) {
		Windmill windmill = poolWindmill.obtain();
		windmill.create(x, y, 2.5f);
		obstacles.add(windmill);
	}

	/**
	 * ITEM
	 */

	private void createCoin(float x, float y, int unit, Pattern pattern) {
		Coins coin = poolCoin.obtain();
		coin.create(x, y, unit, 0.1f, pattern);
		items.add(coin);
	}

	public void removeCoin(Coin coin) {
		for (Component component : items) {
			if (component instanceof Coins) {
				Coins coins = (Coins) component;
				coins.get().removeValue(coin, false);
			}
		}
	}

	private void createFuel(float x, float y) {
		Fuel powerup = poolFuel.obtain();
		powerup.create(x, y);
		items.add(powerup);
	}

	public void removeFuel(Fuel fuel) {
		items.removeValue(fuel, false);
	}

	private void createHeart(float x, float y) {
		Heart heart = poolHeart.obtain();
		heart.create(x, y + 0.02f, 0.05f);
		items.add(heart);
	}

	public void removeHeart(Heart heart) {
		items.removeValue(heart, false);
	}

	private void destroy() {
		if (obstacles.size == 0)
			return;
		// DESTROY OBSTACLE
		Component firstObstacle = obstacles.first();
		if (firstObstacle.getX() + firstObstacle.getWidth() < 0) {
			obstacles.removeValue(firstObstacle, false);
			Pools.free(firstObstacle);
		}

		if (items.size == 0)
			return;
		// DESTROY ITEM
		Component firstItem = items.first();
		if (firstItem.getX() + firstItem.getWidth() < 0) {
			items.removeValue(firstItem, false);
			Pools.free(firstItem);
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		for (Component obstalce : obstacles) {
			obstalce.draw(batch, parentAlpha);
		}
		for (Component item : items) {
			item.draw(batch, parentAlpha);
		}
	}

	@Override
	public void drawDebug(ShapeRenderer shapes) {
		if (!getDebug())
			return;

		shapes.set(ShapeType.Line);
		shapes.setColor(getStage().getDebugColor());
		for (Wall wall : getWalls()) {
			shapes.polygon(wall.getBound().getTransformedVertices());
		}
		for (Block block : getBlocks()) {
			shapes.polygon(block.getBound().getTransformedVertices());
		}
		for (Windmill windmill : getWindmills()) {
			shapes.polygon(windmill.getBound().getTransformedVertices());
		}
		for (Coin coin : getCoins()) {
			shapes.polygon(coin.getBound().getTransformedVertices());
		}
		for (Fuel fuel : getFuels()) {
			shapes.polygon(fuel.getBound().getTransformedVertices());
		}
		for (Heart heart : getHearts()) {
			shapes.polygon(heart.getBound().getTransformedVertices());
		}
		shapes.setColor(Color.BROWN);
		shapes.line(pointer, 0, pointer, World.HEIGHT);

	}

	/**
	 * OBSTACLE
	 */
	// WALL
	private Pool<Walls> poolWall = new Pool<Walls>() {

		@Override
		protected Walls newObject() {
			return new Walls();
		}

	};

	public Array<Wall> getWalls() {
		Array<Wall> walls = new Array<Wall>();
		for (Component component : obstacles) {
			if (component instanceof Walls)
				walls.addAll(((Walls) component).get());
		}
		return walls;
	}

	// BLOCK
	private Pool<Block> poolBlock = new Pool<Block>() {

		@Override
		protected Block newObject() {
			// TODO Auto-generated method stub
			return new Block();
		}

	};

	public Array<Block> getBlocks() {
		Array<Block> blocks = new Array<Block>();
		for (Component component : obstacles) {
			if (component instanceof Block) {
				blocks.add((Block) component);
			}
		}
		return blocks;
	}

	// WINDMILL
	private Pool<Windmill> poolWindmill = new Pool<Windmill>() {

		@Override
		protected Windmill newObject() {
			// TODO Auto-generated method stub
			return new Windmill();
		}

	};

	public Array<Windmill> getWindmills() {
		Array<Windmill> windmills = new Array<Windmill>();
		for (Component component : obstacles) {
			if (component instanceof Windmill)
				windmills.add((Windmill) component);
		}
		return windmills;
	}

	/**
	 * ITEM
	 */
	// COIN
	private Pool<Coins> poolCoin = new Pool<Coins>() {

		@Override
		protected Coins newObject() {
			return new Coins();
		}

	};

	public Array<Coin> getCoins() {
		Array<Coin> coins = new Array<Coin>();
		for (Component component : items) {
			if (component instanceof Coins)
				coins.addAll(((Coins) component).get());
		}
		return coins;
	}

	// POWER UP
	private Pool<Fuel> poolFuel = new Pool<Fuel>() {

		@Override
		protected Fuel newObject() {
			// TODO Auto-generated method stub
			return new Fuel();
		}

	};

	public Array<Fuel> getFuels() {
		Array<Fuel> powerups = new Array<Fuel>();
		for (Component component : items) {
			if (component instanceof Fuel)
				powerups.add((Fuel) component);
		}
		return powerups;
	}

	// HEART
	private Pool<Heart> poolHeart = new Pool<Heart>() {

		@Override
		protected Heart newObject() {
			// TODO Auto-generated method stub
			return new Heart();
		}

	};

	public Array<Heart> getHearts() {
		Array<Heart> hearts = new Array<Heart>();
		for (Component component : items) {
			if (component instanceof Heart)
				hearts.add((Heart) component);
		}
		return hearts;
	}
}
