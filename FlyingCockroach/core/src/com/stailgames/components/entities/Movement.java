package com.stailgames.components.entities;

public class Movement {

	public static float platform = -0.04f;
	public static float environment = -0.02f;
	public static float charging = 10f;
	public static float throwing = -0.04f;
	public static float wall_motion = 0.01f;

	public static void speed(float times) {
		platform = platform * times;
		environment = environment * times;
		charging = charging * times;
		throwing = throwing * times;
		wall_motion = wall_motion * times;
	}

	public static void stopAll() {
		platform = 0.0f;
		environment = 0.0f;
		charging = 0.0f;
	}

	public static void playAll() {
		platform = -0.04f;
		environment = -0.02f;
		charging = 10f;
		throwing = -0.04f;
	}

}
