package com.stailgames.components.entities;

import static com.badlogic.gdx.math.MathUtils.random;
import static com.badlogic.gdx.math.MathUtils.randomBoolean;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.stailgames.components.Blade;
import com.stailgames.components.Block;
import com.stailgames.components.Component;
import com.stailgames.components.Block.TYPE;
import com.stailgames.main.World;

public class Platform extends Component {

	private Pool<Block> poolBlock = new Pool<Block>() {

		@Override
		protected Block newObject() {
			return new Block();
		}
	};

	private Pool<Blade> poolBlade = new Pool<Blade>() {

		@Override
		protected Blade newObject() {
			return new Blade();
		}

	};

	public Array<Blade> getBlades() {
		Array<Blade> blades = new Array<Blade>();
		for (Component component : ground) {
			if (component instanceof Blade)
				blades.add((Blade) component);
		}
		for (Component component : roof) {
			if (component instanceof Blade)
				blades.add((Blade) component);
		}
		return blades;
	}

	private Array<Component> ground = new Array<Component>();
	private Array<Component> roof = new Array<Component>();
	private boolean active;

	public Platform() {
		super("Platform");
		active = true;
		// DEFAULT
		float x = 0.0f;
		Block block = null;
		do {
			// INITIAL GROUND
			block = poolBlock.obtain();
			block.create(x, 0, TYPE.PLATFORM);
			ground.add(block);
			// INITIAL ROOF
			block = poolBlock.obtain();
			block.create(x, World.HEIGHT, true, TYPE.PLATFORM);
			roof.add(block);

			x += block.getWidth();

		} while (x < World.WIDTH);
	}

	public void activated(boolean state) {
		active = state;
	}

	public void update(float delta) {
		create();
		// UPDATE GROUND
		for (Component component : ground) {
			component.act(delta);
		}
		// UPDATE ROOF
		for (Component component : roof) {
			component.act(delta);
		}
		destroy();
	}

	private void create() {
		Block block = null;
		Blade blade = null;

		// CREATE GROUND
		Component lastGround = ground.peek();
		if (lastGround.getX() + lastGround.getWidth() < World.WIDTH) {
			if (!active) {
				// DEFAULT
				block = poolBlock.obtain();
				block.create(lastGround.getX() + lastGround.getWidth(), 0, TYPE.PLATFORM);
				ground.add(block);
			} else {
				// CREATE BLOCK IF LAST GROUND IS BLADE
				if (lastGround instanceof Blade) {
					block = poolBlock.obtain();
					block.create(lastGround.getX() + lastGround.getWidth(), 0, TYPE.PLATFORM);
					ground.add(block);
				} else {
					// CREATE RANDOM GROUND
					if (randomBoolean()) {
						blade = poolBlade.obtain();
						blade.create(lastGround.getX() + lastGround.getWidth(), 0, random(2, 8));
						ground.add(blade);
					} else {
						block = poolBlock.obtain();
						block.create(lastGround.getX() + lastGround.getWidth(), 0, TYPE.PLATFORM);
						ground.add(block);
					}
				}
			}
		}

		// CREATE ROOF
		Component lastRoof = roof.peek();
		if (lastRoof.getX() + lastRoof.getWidth() < World.WIDTH) {
			if (!active) {
				// DEFAULT
				block = poolBlock.obtain();
				block.create(lastRoof.getX() + lastRoof.getWidth(), World.HEIGHT, true, TYPE.PLATFORM);
				roof.add(block);
			} else {
				// CREATE BLOCK IF LAST ROOF IS BLADE
				if (lastRoof instanceof Blade) {
					block = poolBlock.obtain();
					block.create(lastRoof.getX() + lastRoof.getWidth(), World.HEIGHT, true, TYPE.PLATFORM);
					roof.add(block);
				} else {
					// CREATE RANDOM ROOF
					if (randomBoolean()) {
						blade = poolBlade.obtain();
						blade.create(lastRoof.getX() + lastRoof.getWidth(), World.HEIGHT, random(2, 8), true);
						roof.add(blade);
					} else {
						block = poolBlock.obtain();
						block.create(lastRoof.getX() + lastRoof.getWidth(), World.HEIGHT, true, TYPE.PLATFORM);
						roof.add(block);
					}
				}
			}
		}
	}

	private void destroy() {
		// DESTROY GROUND
		Component firstGround = ground.first();
		if (firstGround.getX() + firstGround.getWidth() < 0) {
			ground.removeValue(firstGround, false);
			Pools.free(firstGround);
		}

		// DESTROY ROOF
		Component firstRoof = roof.first();
		if (firstRoof.getX() + firstRoof.getWidth() < 0) {
			roof.removeValue(firstRoof, false);
			Pools.free(firstRoof);
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		// DRAW GROUND
		for (Component component : ground) {
			component.draw(batch, parentAlpha);
		}
		// DRAW ROOF
		for (Component component : roof) {
			component.draw(batch, parentAlpha);
		}
	}

	@Override
	public void drawDebug(ShapeRenderer shapes) {
		if (!getDebug())
			return;
		shapes.set(ShapeType.Line);
		shapes.setColor(getStage().getDebugColor());
		for (Component component : ground) {
			shapes.polygon(((Component) component).getBound().getTransformedVertices());
		}
		for (Component component : roof) {
			shapes.polygon(((Component) component).getBound().getTransformedVertices());
		}
	}

	public float getLowerLimit() {
		return Block.HEIGHT;
	}

	public float getUpperLimit() {
		return World.HEIGHT - Block.HEIGHT;
	}
}
