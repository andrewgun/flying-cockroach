package com.stailgames.components;

import static com.stailgames.utils.Converter.pixelToMeter;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.stailgames.components.entities.Movement;
import com.stailgames.manager.R;

public class Fuel extends Component implements Poolable {

	// UTILITIES
	private static void drawCenter(Batch batch, TextureRegion region, float x, float y, float width, float height) {
		batch.draw(region, x - width / 2, y - height / 2, width, height);
	}

	public static final float WIDTH = pixelToMeter(29);
	public static final float HEIGHT = pixelToMeter(17);

	public float x, y;

	public Fuel() {
		super("Power-Up");
		x = 0.0f;
		y = 0.0f;
	}

	public void create(float x, float y) {
		this.x = x;
		this.y = y;
		setBoundingRectangle(x - WIDTH / 2, y - HEIGHT / 2, WIDTH, HEIGHT);
	}

	@Override
	public void update(float delta) {
		x += Movement.environment;

		setBoundingRectangle(x - WIDTH / 2, y - HEIGHT / 2, WIDTH, HEIGHT);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		drawCenter(batch, R.assets.image("power_up"), x, y, WIDTH, HEIGHT);
	}
	
	@Override
	public boolean equals(Object fuel) {
		return this.x == ((Fuel) fuel).x && this.y == ((Fuel) fuel).y;
	}

	@Override
	public void reset() {
		x = 0.0f;
		y = 0.0f;
	}

}
