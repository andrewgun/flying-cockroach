package com.stailgames.components;

import static com.stailgames.utils.Converter.pixelToMeter;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.stailgames.components.entities.Movement;
import com.stailgames.manager.R;

public class Block extends Component implements Poolable {

	// UTILITIES
	private static void drawCenter(Batch batch, TextureRegion reg, float x, float y, float width, float height) {
		batch.draw(reg, x - width / 2, y - height / 2, width, height);
	}

	public static final float WIDTH = pixelToMeter(300);
	public static final float HEIGHT = pixelToMeter(50);

	public static enum TYPE {
		ENVIRONMENT, PLATFORM
	}

	public float x, y;
	public boolean flip;
	public TYPE type;

	private float scaleX, scaleY;

	public Block() {
		super("Block");
		x = 0.0f;
		y = 0.0f;
		scaleX = 1.0f;
		scaleY = 1.0f;
		flip = false;
	}

	public void setScale(float scaleX, float scaleY) {
		this.scaleX = scaleX;
		this.scaleY = scaleY;
	}

	public void create(float x, float y, TYPE type) {
		create(x, y, false, type);
	}

	public void create(float x, float y, boolean flip, TYPE type) {
		this.x = x;
		this.y = y;
		this.flip = flip;
		this.type = type;
		if (type == TYPE.ENVIRONMENT)
			setBoundingCenter(x, y, WIDTH * scaleX, flip ? -(HEIGHT * scaleY) : (HEIGHT * scaleY));
		if (type == TYPE.PLATFORM)
			setBoundingRectangle(x, y, WIDTH * scaleX, flip ? -(HEIGHT * scaleY) : (HEIGHT * scaleY));
	}

	@Override
	public void update(float delta) {
		if (type == TYPE.ENVIRONMENT) {
			x += Movement.environment;
			setBoundingCenter(x, y, WIDTH * scaleX, flip ? -(HEIGHT * scaleY) : (HEIGHT * scaleY));
		}
		if (type == TYPE.PLATFORM) {
			x += Movement.platform;
			setBoundingRectangle(x, y, WIDTH * scaleX, flip ? -(HEIGHT * scaleY) : (HEIGHT * scaleY));
		}

	}

	private void setBoundingCenter(float x, float y, float width, float height) {
		setBoundingRectangle(x - width / 2, y - height / 2, width, height);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		if (type == TYPE.ENVIRONMENT)
			drawCenter(batch, R.assets.image("block"), x, y, WIDTH * scaleX, flip ? -(HEIGHT * scaleY) : (HEIGHT * scaleY));
		if (type == TYPE.PLATFORM)
			batch.draw(R.assets.image("block"), x, y, WIDTH * scaleX, flip ? -(HEIGHT * scaleY) : (HEIGHT * scaleY));
	}

	@Override
	public boolean equals(Object block) {
		return this.x == ((Block) block).x && this.y == ((Block) block).y;
	}

	@Override
	public void reset() {
		x = 0.0f;
		y = 0.0f;
		scaleX = 1.0f;
		scaleY = 1.0f;
	}

}
