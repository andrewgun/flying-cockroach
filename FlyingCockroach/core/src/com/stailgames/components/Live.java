package com.stailgames.components;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.stailgames.manager.R;

public class Live {

	private static final float WIDTH = 26;
	private static final float HEIGHT = 22;
	private static final float MAX_LIVE = 3;

	public float x, y;
	public float live;
	public float offset;

	public Live(float x, float y, float offset) {
		this.x = x;
		this.y = y;
		this.offset = offset;
		live = MAX_LIVE;
	}

	public void set(float value) {
		live = value;
	}

	public void decrease(float amount) {
		live -= amount;
		live = live > 0.0f ? live : 0.0f;
	}

	public void increase(float amount) {
		live += amount;
		live = live < MAX_LIVE ? live : MAX_LIVE;
	}

	public void draw(Batch batch) {
		float xHeart = x;
		for (int i = 0; i < (int) (live / 1.0f); i++) {
			batch.draw(R.assets.image("heart"), xHeart + 2, y + 2, WIDTH - 2, HEIGHT - 2);
			xHeart += (WIDTH + offset);
		}

		for (int i = 0; i < (int) ((live % 1.0f) / 0.5f); i++) {
			batch.draw(R.assets.image("half_heart"), xHeart + 2, y + 2, WIDTH / 2 - 1, HEIGHT - 2);
			xHeart += (WIDTH + offset);
		}

		float xBorder = x;
		for (int i = 0; i < MAX_LIVE; i++) {
			batch.draw(R.assets.image("empty_heart"), xBorder, y, WIDTH, HEIGHT);
			xBorder += (WIDTH + offset);
		}
	}

	public float getLive() {
		return live;
	}


}
