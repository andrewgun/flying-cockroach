package com.stailgames.components;

import static com.stailgames.utils.Converter.pixelToMeter;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.stailgames.components.entities.Movement;
import com.stailgames.manager.R;

public class Windmill extends Component implements Poolable {

	// UTILITIES
	private static void drawCenter(Batch batch, TextureRegion region, float x, float y, float width, float height, float rotation) {
		batch.draw(region, x - width / 2, y - height / 2, width / 2, height / 2, width, height, 1f, 1f, rotation);
	}

	private static final float WIDTH = pixelToMeter(200);
	private static final float HEIGHT = pixelToMeter(50);

	public float x, y;
	public float rotation;

	public Windmill() {
		super("Windmill");
		x = 0.0f;
		y = 0.0f;
		rotation = 0.0f;
	}

	public void create(float x, float y, float rotation) {
		this.x = x;
		this.y = y;
		this.rotation = rotation;

		setBoundingRectangle(x - WIDTH / 2, (y - HEIGHT / 2) + 0.1f, WIDTH, HEIGHT - 0.2f);
	}

	@Override
	public void update(float delta) {
		x += Movement.environment;

		angle += rotation;

		setBoundingRectangle(x - WIDTH / 2, (y - HEIGHT / 2) + 0.02f, WIDTH, HEIGHT - 0.04f);
		setBoundRotation(angle);
	}

	private float angle = 0.0f;

	@Override
	public void draw(Batch batch, float parentAlpha) {
		drawCenter(batch, R.assets.image("windmill"), x, y, WIDTH, HEIGHT, angle);
	}

	@Override
	public void reset() {
		x = 0.0f;
		y = 0.0f;
		rotation = 0.0f;
	}

}
