package com.stailgames.components;

import static com.stailgames.utils.Converter.pixelToMeter;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.stailgames.components.entities.Movement;
import com.stailgames.manager.R;

public class Coins extends Component {

	public enum Pattern {
		LINE, CURVE;

		public static Pattern random() {
			return values()[MathUtils.random(0, values().length - 1)];
		}
	}

	private Pool<Coin> poolCoin = new Pool<Coin>() {

		@Override
		protected Coin newObject() {
			return new Coin();
		}

	};

	private Array<Coin> coins = new Array<Coin>();
	private float x, y;
	public int unit;
	public float offset;

	public Coins() {
		super("Coins");
	}

	public void create(float x, float y, int unit, float offset, Pattern pattern) {
		// SET
		this.x = x;
		this.y = y;
		this.unit = unit;
		this.offset = offset;

		float xCoin = x - width() / 2;
		float yCoin = y - height() / 2;

		switch (pattern) {
		case LINE:
			for (int i = 0; i < unit; i++) {
				Coin coin = poolCoin.obtain();
				coin.create(xCoin, yCoin, 0.1f);
				coins.add(coin);
				xCoin += (Coin.WIDTH + offset);
			}
			break;
		case CURVE:
			for (int i = 0; i < unit; i++) {
				Coin coin = poolCoin.obtain();
				coin.create(xCoin, yCoin, 0.1f);
				coins.add(coin);
				xCoin += (Coin.WIDTH + offset);
				yCoin += (i == 0) ? offset : (i == unit - 2) ? -offset : 0.0f;
			}
			break;
		}

		setBoundingRectangle(x - width() / 2, y - height() / 2, width(), height());
	}

	public float width() {
		return (Coin.WIDTH * unit + offset * (unit - 1));
	}

	public float height() {
		return Coin.HEIGHT;
	}

	@Override
	public void update(float delta) {
		x += Movement.environment;

		for (Coin coin : coins) {
			coin.update(delta);
		}

		setBoundingRectangle(x - width() / 2, y - height() / 2, width(), height());
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		for (Coin coin : coins) {
			coin.draw(batch, parentAlpha);
		}
	}

	public Array<Coin> get() {
		return coins;
	}

	public static class Coin extends Component implements Poolable {

		public static final float WIDTH = pixelToMeter(32);
		public static final float HEIGHT = pixelToMeter(32);

		public float x, y;
		public float scale, delay;

		public Coin() {
			super("Coin");
			x = 0.0f;
			y = 0.0f;
			scale = 1.0f;
			delay = 0.0f;
		}

		private void create(float x, float y, float delay) {
			this.x = x;
			this.y = y;
			this.delay = delay;

			setBoundingRectangle(x, y, WIDTH, HEIGHT);
		}

		@Override
		public void draw(Batch batch, float parentAlpha) {
			batch.draw(R.assets.image("coin"), x, y, WIDTH / 2, HEIGHT / 2, WIDTH, HEIGHT, scale, 1.0f, 0.0f);
		}

		private float tick = 0.0f;

		@Override
		public void update(float delta) {
			x += Movement.environment;

			if ((tick += delta) > delay) {

				scale = scale > -1f ? scale - 0.1f : 1.0f;

				tick = 0.0f;
			}

			setBoundingRectangle(x, y, WIDTH, HEIGHT);
		}

		@Override
		public boolean equals(Object coin) {
			return this.x == ((Coin) coin).x && this.y == ((Coin) coin).y;
		}

		@Override
		public void reset() {
			x = 0.0f;
			y = 0.0f;
			scale = 1.0f;
			delay = 0.0f;
		}

	}
}
