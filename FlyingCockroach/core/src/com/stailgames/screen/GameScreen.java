package com.stailgames.screen;

import static com.stailgames.utils.Converter.colorByteToFloat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.stailgames.components.entities.Cockroach;
import com.stailgames.components.entities.Detector;
import com.stailgames.components.entities.Environment;
import com.stailgames.components.entities.HUD;
import com.stailgames.components.entities.Movement;
import com.stailgames.components.entities.Platform;
import com.stailgames.components.entities.ThrowingObject;
import com.stailgames.debug.CameraHelper;
import com.stailgames.debug.Info;
import com.stailgames.main.World;

public class GameScreen extends AbstractScreen {

	private static boolean DEBUG = false;
	private int timer = 0;
	private float tick = 0.0f;

	// STAGE
	private Stage stage;

	// ACTOR
	private Cockroach cockroach;
	private Platform platform;
	private Environment environment;
	private ThrowingObject throwing;

	// HUD
	private HUD HUD;

	// COLLISION DETECTION
	private Detector detector;

	@Override
	public void show() {
		// INITIALIZE STAGE
		stage = new Stage(new FillViewport(World.WIDTH, World.HEIGHT));

		// INITIALIZE ACTOR
		cockroach = new Cockroach();

		platform = new Platform();
		platform.activated(true);

		environment = new Environment();
		environment.activated(true);

		throwing = new ThrowingObject(10f, cockroach);

		stage.addActor(environment);
		stage.addActor(cockroach);
		stage.addActor(platform);
		stage.addActor(throwing);

		stage.setDebugAll(DEBUG);

		// INITIALIZE HUD
		HUD = new HUD();

		// COLLISION DETECTION
		detector = new Detector(cockroach, environment, platform, HUD, throwing);

	}

	@Override
	public void render(float delta) {

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		Gdx.gl.glClearColor(colorByteToFloat(190), colorByteToFloat(241), colorByteToFloat(255), colorByteToFloat(255));

		detector.response(delta);

		stage.act(delta);
		stage.draw();

		HUD.render();

		// TIMER
		if ((tick += delta) > 1f) {
			timer++;
			tick = 0.0f;
		}
		// DEBUGGING
		debugController();
		CameraHelper.update((OrthographicCamera) stage.getCamera());
		Info.drawCamera(stage);
		String names = "";
		for (Actor actor : stage.getActors()) {
			if (actor != null)
				names += "[" + actor.getName() + "," + actor.getZIndex() + "]";
		}

		Info.drawString(names, 300, 5);
		Info.drawString("TIME : " + timer, 600, 20);
		Info.render();
	}

	private void debugController() {
		if (Gdx.input.isKeyJustPressed(Keys.P)) {
			Movement.speed(1.2f);
		}
		if (Gdx.input.isKeyJustPressed(Keys.L)) {
			Movement.speed(0.8f);
		}
		if (Gdx.input.isKeyJustPressed(Keys.O)) {
			stage.setDebugAll(DEBUG = !DEBUG);
			Info.VISIBLE = !Info.VISIBLE;
		}

		Info.drawString("PRESS P : INCREASE SPEED", 500, 50);
		Info.drawString("PRESS O : DEBUG MODE (ON/OFF)", 500, 60);
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, false);
		HUD.resize(width, height);
	}

	@Override
	public void pause() {

	}

	@Override
	public void hide() {

	}
}
