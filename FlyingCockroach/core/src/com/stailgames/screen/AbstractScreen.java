package com.stailgames.screen;

import com.badlogic.gdx.Screen;
import com.stailgames.manager.R;

public abstract class AbstractScreen implements Screen {

	@Override
	abstract public void show();

	@Override
	abstract public void render(float delta);

	@Override
	abstract public void resize(int width, int height);

	@Override
	abstract public void pause();

	@Override
	public void resume() {
		R.assets.load();
	}

	@Override
	abstract public void hide();

	@Override
	public void dispose() {
		R.assets.dispose();
	}

}
