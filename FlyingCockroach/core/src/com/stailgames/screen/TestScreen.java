package com.stailgames.screen;

import static com.stailgames.utils.Converter.colorByteToFloat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.stailgames.components.Warning;
import com.stailgames.debug.CameraHelper;
import com.stailgames.debug.Info;
import com.stailgames.main.World;

public class TestScreen extends AbstractScreen {

	private Warning warning;
	private Stage stage;

	@Override
	public void show() {
		stage = new Stage(new FillViewport(World.WIDTH, World.HEIGHT));

		warning = new Warning();
		warning.create(World.WIDTH / 2, World.HEIGHT / 2, 2f);

		stage.addActor(warning);
		stage.setDebugAll(false);

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		Gdx.gl.glClearColor(colorByteToFloat(190), colorByteToFloat(241), colorByteToFloat(255), colorByteToFloat(255));

		stage.act(delta);
		stage.draw();

		System.out.println(warning.isOver());

		CameraHelper.update((OrthographicCamera) stage.getCamera());
		Info.drawCamera(stage);
		Info.render();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, false);
	}

	@Override
	public void pause() {
	}

	@Override
	public void hide() {
	}

}
