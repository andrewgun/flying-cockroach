package com.stailgames.utils;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pool.Poolable;

public class Cycle {

	private static Pool<Execute> pool = new Pool<Execute>() {
		@Override
		protected Execute newObject() {
			return new Execute();
		}
	};
	private static Array<Execute> executes = new Array<Execute>();

	private static float delta;
	private static int cycle;

	public static void SYNC(float deltaTime) {
		delta = deltaTime;
		cycle = 0;
	}

	public static boolean ONCE() {
		cycle++;
		if (cycle - 1 == executes.size)
			executes.add(pool.obtain());
		return executes.get(cycle - 1).once();
	}

	public static boolean EVERY(float delay) {
		cycle++;
		if (cycle - 1 == executes.size)
			executes.add(pool.obtain());
		return executes.get(cycle - 1).every(delay);
	}

	public static boolean ONCE(float delay) {
		cycle++;
		if (cycle - 1 == executes.size)
			executes.add(pool.obtain());
		return executes.get(cycle - 1).every(delay) ? executes.get(cycle - 1).once() : false;
	}
	
	public static void CLEAR(){
		executes.clear();
	}

	private static class Execute implements Poolable {

		private boolean state = true;
		private float timer = 0.0f;

		private boolean once() {
			if (state) {
				state = false;
				return true;
			}
			return false;
		}

		private boolean every(float delay) {
			if ((timer += delta) > delay) {
				timer = 0.0f;
				return true;
			}
			return false;
		}

		@Override
		public void reset() {
			cycle = -1;
			state = true;
			timer = 0.0f;
		}

	}
}
