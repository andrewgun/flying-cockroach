package com.stailgames.flyingcockroach.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;
import com.stailgames.main.Main;

public class DesktopLauncher {

	private static final boolean IN_BUILT = true;
	private static final boolean DEBUG = false;

	public static void main(String[] arg) {
		if (IN_BUILT) {
			Settings settings = new Settings();
			settings.maxWidth = 1024;
			settings.maxHeight = 1024;
			settings.minWidth = 512;
			settings.minHeight = 512;
			settings.filterMin = TextureFilter.Nearest;
			settings.filterMag = TextureFilter.Nearest;
			settings.paddingX = 4;
			settings.paddingY = 4;
			settings.debug = DEBUG;
			TexturePacker.process(settings, 
					"assets-raw/images",
					"D:/Penting/LibGDX/Game LibGDX Project/FlyingCockroach/android/assets", 
					"flyingcockroach");
		}

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 800;
		config.height = 480;
		config.resizable = false;
		new LwjglApplication(new Main(), config);
	}
}
